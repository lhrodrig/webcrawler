from jobs.brokenLinks import checkBrokenLinks
from utils.cute_shell import printBanner
import pandas as pd
import os

def writeFile(dataframe, file):
    dirname = os.path.dirname(__file__)
    dest = os.path.join(dirname, 'output', file.split('.')[0] + "_checked.xlsx")
    dataframe.to_excel(dest)


def main():
    printBanner()
    menu_option = input("Select option: ")
    if menu_option == '1':
        file = input("broken links file name: ")
        data = checkBrokenLinks(file)
        writeFile(data,file)
    else: 
        print('not implemente yet \n bye!')
    


if __name__ == "__main__":
    main()