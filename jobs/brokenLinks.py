import imp
from tabulate import tabulate
from tld import get_tld
from playwright.sync_api import sync_playwright
from services.rotatorLinkService import *
from utils.cute_shell import printProgressBar
import re
import pandas as pd

def openCsv(file):
    data_import = pd.read_csv(file,     
                          names = ['linkId', 'errorCode', 'errorDescription'])
    
    data_import.fillna('', inplace=True)
    data_import.applymap(lambda x: x.strip() if isinstance(x, str) else x)
    print(tabulate(data_import.head(), headers='keys', tablefmt='psql'))
    return data_import

def extractDomain(url):
    pat = r'((https?):\/\/)'
    m = re.match(pat, url)
    http_url = url if m else "http://"+url
    res = get_tld(http_url, as_object=True)
    return res.fld 


def checkLastRedirect(url):
    last_url = ""
    with sync_playwright() as p:
        browser = p.chromium.launch()
        context = browser.new_context(ignore_https_errors=True)
        context.set_default_navigation_timeout(80000)
        page = context.new_page()
        page.goto(url, wait_until="domcontentloaded")
        
        last_url = page.url
        browser.close()
    return last_url

def checkBrokenLinks(file):
    data = openCsv(file)
    print()
    print("***** PROCESSING *****")
    print()
    for row, elem in data.iterrows():
        extra_info = getRotatorLinksActive(str(elem['linkId']))
        last_url = checkLastRedirect(extra_info[elem['linkId']]['url'])
        broken_link = 'good' if extractDomain(last_url) in extractDomain(extra_info[elem['linkId']]['landingPage']) else 'wrong'
        data.loc[row,'landingUrl'] = extra_info[elem['linkId']]['landingPage']
        data.loc[row,'testedUrl'] = last_url
        data.loc[row,'status'] = broken_link
        printProgressBar(row + 1, len(data), prefix = 'Progress:', suffix = 'Complete', length = 50)
    return data



